<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<%@taglib prefix="t" tagdir="/WEB-INF/tags/templates" %>
<t:layout>
	<jsp:attribute name="styles">
		<!-- put your styles here -->
	</jsp:attribute>
	<jsp:attribute name="scripts">
		<!-- put your scripts here -->
		<script type="text/javascript" src="scripts/person/PersonViewModel.js"></script>
		<script type="text/javascript">
			$(function(){
				var model = {
					name:'',
					age:0
				};
				var viewModel = new PersonViewModel(model);
				ko.applyBindings(viewModel);
				
			});	
		</script>
	</jsp:attribute>
	<jsp:body>
		Add person:<br/>
		<label>Name <input type="text" data-bind="value: name"/></label><br/>
		<label>Age <input type="text" data-bind="value: age"/></label><br/>		
		<button data-bind="click:show">Show</button>
		<button data-bind="click:add">Add</button>
	</jsp:body>
</t:layout>